package com.ahantiev;

import static org.junit.Assert.assertTrue;

import com.ahantiev.common.TimeProvider;
import com.ahantiev.executor.Executor;
import com.ahantiev.executor.Task;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

public class ExecutorTest {
    private static final Logger log = LogManager.getLogger(ExecutorTest.class);
    private LocalDateTime now;

    @Before
    public void setUp() {
        now = LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC);
    }

    @Test(expected = IllegalArgumentException.class)
    public void executorInitializationFail() {
        new Executor(0);
    }

    @Test(expected = NullPointerException.class)
    public void nullableFirstParam() {
        Executor executor = new Executor(1);
        executor.start();
        executor.submit(null, now);
    }

    @Test(expected = NullPointerException.class)
    public void nullableSecondParam() {
        Executor executor = new Executor(1);
        executor.start();
        executor.submit(shortTask(), null);
    }

    @Test(expected = NullPointerException.class)
    public void nullableBothParams() {
        Executor executor = new Executor(1);
        executor.start();
        executor.submit(null, null);
    }

    @Test
    public void ordering() throws InterruptedException, ExecutionException {
        TimeProvider.useFixedClockAt(now);

        Executor executor = new Executor(1);
        executor.start();

        List<Task<?>> firstPart = IntStream.range(0, 100).parallel()
                .mapToObj(value -> executor.submit(shortTask(), now.plusSeconds(2)))
                .collect(Collectors.toList());

        Thread.sleep(1000);
        log.info("--------------------------------------");
        log.info("after 1 second(s)");
        log.info("--------------------------------------");
        TimeProvider.useFixedClockAt(now.plusSeconds(5));

        List<Task<?>> secondPart = IntStream.range(0, 100).parallel()
                .mapToObj(value -> executor.submit(shortTask(), now.plusSeconds(1)))
                .collect(Collectors.toList());

        Thread.sleep(2000);
        log.info("--------------------------------------");
        log.info("after 2 second(s)");
        log.info("--------------------------------------");
        TimeProvider.useFixedClockAt(now.plusSeconds(6));

        for (Task<?> task : firstPart) {
            task.get();
            assertTrue(task.isDone());
        }
        for (Task<?> task : secondPart) {
            task.get();
            assertTrue(task.isDone());
        }
        executor.getStatistic().print();
    }

    @Test
    public void orderingWithRealTime() throws InterruptedException, ExecutionException {
        now = TimeProvider.now();

        Executor executor = new Executor(1);
        executor.start();

        List<Task<?>> tasks = IntStream.range(0, 100).parallel()
                .mapToObj(value -> executor.submit(shortTask(), now.plusSeconds(1)))
                .collect(Collectors.toList());
        executor.getStatistic().print();

        for (Task<?> task : tasks) {
            task.get();
            assertTrue(task.isDone());
        }

        executor.getStatistic().print();
    }

    @Test
    public void orderingWithLongTask() throws InterruptedException, ExecutionException {
        TimeProvider.useFixedClockAt(now);

        Executor executor = new Executor(1);
        executor.start();

        Task<?> first = executor.submit(longTask(), now.plusSeconds(2));
        Task<?> second = executor.submit(longTask(), now.plusSeconds(1));

        TimeProvider.useFixedClockAt(now.plusSeconds(2));

        first.get();
        second.get();
        executor.getStatistic().print();
    }

    @Test
    public void multiplyWorkers() throws InterruptedException, ExecutionException {
        TimeProvider.useFixedClockAt(now);

        Executor executor = new Executor(4);
        executor.start();

        List<Task<?>> tasks = IntStream.range(0, 100).parallel()
                .mapToObj(value -> executor.submit(shortTask(), now.plusSeconds(1)))
                .collect(Collectors.toList());

        executor.getStatistic().print();

        TimeProvider.useFixedClockAt(now.plusSeconds(2));

        for (Task<?> task : tasks) {
            task.get();
            assertTrue(task.isDone());
        }

        executor.getStatistic().print();
    }

    @Test(expected = CancellationException.class)
    public void cancellation() throws InterruptedException, ExecutionException {
        TimeProvider.useFixedClockAt(now);

        Executor executor = new Executor(1);
        executor.start();

        Task<?> future = executor.submit(shortTask(), now.plusSeconds(1));
        future.cancel(true);

        TimeProvider.useFixedClockAt(now.plusSeconds(2));

        future.get();
        executor.getStatistic().print();
    }

    @Test(expected = ExecutionException.class)
    public void taskFailing() throws ExecutionException, InterruptedException {
        TimeProvider.useFixedClockAt(now);

        Executor executor = new Executor(1);
        executor.start();

        Task<?> future = executor.submit(failingTask(), now.plusSeconds(1));

        TimeProvider.useFixedClockAt(now.plusSeconds(2));

        future.get();
        executor.getStatistic().print();
    }

    @Test
    @SuppressWarnings("StatementWithEmptyBody")
    public void interrupting() throws InterruptedException {
        Executor executor = new Executor(1);
        executor.start();

        Thread.sleep(1000);

        executor.interrupt();

        while (executor.isRunning()) {
        }
    }

    private Callable<?> shortTask() {
        return () -> null;
    }

    private Callable<?> longTask() {
        return () -> {
            Thread.sleep(5000);
            return null;
        };
    }

    private Callable<?> failingTask() {
        return () -> {
            throw new RuntimeException("Fail!");
        };
    }
}
