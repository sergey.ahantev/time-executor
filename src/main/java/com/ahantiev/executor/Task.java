package com.ahantiev.executor;

import java.time.LocalDateTime;
import java.util.concurrent.Delayed;
import java.util.concurrent.RunnableFuture;

public interface Task<V> extends Delayed, RunnableFuture<V> {
    long getId();

    LocalDateTime getStartTime();
}
