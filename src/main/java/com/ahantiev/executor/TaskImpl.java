package com.ahantiev.executor;

import com.ahantiev.common.TimeProvider;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Callable;
import java.util.concurrent.Delayed;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

class TaskImpl<V> extends FutureTask<V> implements Task<V> {

    private final LocalDateTime startTime;
    private final long id;

    TaskImpl(Callable<V> callable, LocalDateTime startTime, long id) {
        super(callable);
        this.startTime = startTime;
        this.id = id;
    }

    @Override
    public int compareTo(Delayed o) {
        if (o == this) {
            return 0;
        }

        if (o instanceof Task) {
            Task other = (Task) o;
            if (startTime.isBefore(other.getStartTime())) {
                return -1;
            } else if (startTime.isAfter(other.getStartTime())) {
                return 1;
            } else {
                return Long.compare(this.getId(), other.getId());
            }
        }

        long diff = getDelay(NANOSECONDS) - o.getDelay(NANOSECONDS);
        if (diff < 0) {
            return -1;
        } else {
            return (diff > 0) ? 1 : 0;
        }
    }

    @Override
    public LocalDateTime getStartTime() {
        return startTime;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        long diff = TimeProvider.now().until(startTime, ChronoUnit.NANOS);
        return unit.convert(diff, TimeUnit.NANOSECONDS);
    }

}
