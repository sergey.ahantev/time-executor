package com.ahantiev.executor;

import com.ahantiev.common.TimeProvider;
import java.util.concurrent.BlockingQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Worker extends Thread {
    private static final Logger log = LogManager.getLogger(Worker.class);

    private final BlockingQueue<Task> sharedQueue;
    private final Statistic statistic;

    public Worker(String name, BlockingQueue<Task> sharedQueue, Statistic statistic) {
        super(name);
        this.statistic = statistic;
        this.sharedQueue = sharedQueue;
        this.setDaemon(true);
    }

    @Override
    public void run() {
        while (!this.isInterrupted()) {
            try {
                Task task = this.sharedQueue.take();
                if (task.isCancelled()) {
                    log.trace("{} Task: {} is cancelled", getName(), task.getId());
                    statistic.canceledCount.incrementAndGet();
                } else {
                    executeTask(task);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(String.format("%s interrupted", getName()), e);
            }
        }
    }

    private void executeTask(Task task) {
        try {
            task.run();
            statistic.successfulCount.incrementAndGet();
            log.trace("{} Task: {}, startTime: {}, currentTime: {} executed",
                    getName(),
                    task.getId(),
                    task.getStartTime(),
                    TimeProvider.now());
        } catch (Throwable t) {
            statistic.failedCount.incrementAndGet();
            throw t;
        }

    }
}
